package sfaxx.rndnamegen;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;

public class RndNameGen
{
	private Random rand;
	
	private ArrayList<Character> characters;
	private int[] charactersWeights;
	private int charactersTotalWeight;
	private int[] vowesIndexes;
	private int[] vowesWeights;
	private int vowesTotalWeight;
	private int[] sizes;
	private int[] sizesWeights;
	private int sizesTotalWeight;
	
	private int[][] charOrderWeights;
	private int[] charOrderTotalWeights;
	
	public RndNameGen()
	{
		this.rand = new Random();
		this.characters = new ArrayList<>();
		this.charactersTotalWeight = 0;
		this.sizesTotalWeight = 0;
		loadData();
	}
	
	private void loadData()
	{
		InputStream charmap = Main.class.getResourceAsStream("/assets/rndnamegen/files/charmap.map");
		String line = "";
		
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(charmap)))
		{
			boolean sizesSegment = false;
			
			LinkedList<IntPair> sizes = new LinkedList<>();
			
			int i = 0;
			int j = 0;
			
			while(null != (line = reader.readLine()))
			{
				String[] lineValues = line.split(",");
				
				if(0 == i)
				{
					LinkedList<Integer> vowes = new LinkedList<>();
					for(int c = 0; c < lineValues.length; c++)
					{
						characters.add(lineValues[c].charAt(0));
						
						if(isVowel(characters.get(c)))
						{
							vowes.add(c);
						}
					}
					
					this.vowesIndexes = new int[vowes.size()];
					this.vowesWeights = new int[vowes.size()];					
					this.charactersWeights = new int[characters.size()];
					this.charOrderWeights = new int[characters.size()][characters.size()];
					this.charOrderTotalWeights = new int[characters.size()];
					
					ListIterator<Integer> iter = vowes.listIterator();
					
					while(iter.hasNext())
					{
						this.vowesIndexes[iter.nextIndex()] = iter.next();
					}
				}
				else if(1 == i)
				{
					for(int n = 0; n < lineValues.length; ++n)
					{
						charactersWeights[n] = Integer.parseInt(lineValues[n]);
						charactersTotalWeight += charactersWeights[n];
					}
						
					for(int v = 0; v < vowesIndexes.length; ++v)
					{
						vowesWeights[v] = charactersWeights[vowesIndexes[v]] / 20;
						vowesTotalWeight += vowesWeights[v];
					}
				}
				
				if(line.contains("<"))
				{
					sizesSegment = true;
					continue;
				}
				else if(line.contains(">"))
				{
					sizesSegment = false;
					++i;
					continue;
				}
				
				if(sizesSegment)
				{
					sizes.add(new IntPair(Integer.parseInt(lineValues[0]), Integer.parseInt(lineValues[1])));
					continue;
				}
				else if (i > 2)
				{
					for(int c = 0; c < lineValues.length; c++)
					{
						charOrderWeights[j][c] = Integer.parseInt(lineValues[c]);
						charOrderTotalWeights[j] += charOrderWeights[j][c];
					}
					
					++j;
				}
				
				++i;
			}
			
			this.sizes = new int[sizes.size()];
			this.sizesWeights = new int[sizes.size()];
			
			ListIterator<IntPair> iter = sizes.listIterator();
			
			int p = 0;
			
			while(iter.hasNext())
			{
				IntPair ip = iter.next(); 
				this.sizes[p] = ip.getLeft();
				this.sizesWeights[p] = ip.getRight();
				this.sizesTotalWeight += this.sizesWeights[p];
				++p;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public String getRandomName()
	{
		int size = getSize();
		int weight = charactersTotalWeight - (size < 4 ? charactersWeights[charactersWeights.length - 1] + charactersWeights[charactersWeights.length - 2] : 0);
		char[] nameChars = new char[size];
		
		int i = 0;
		int consonantCount = 0;
		
		nameChars[i++] = characters.get(getNextWeightedInt(charactersWeights, charactersTotalWeight)).charValue();
		
		for(;i < size; ++i)
		{
			if(isVowel(nameChars[i - 1]))
			{
				consonantCount = 0;
			}
			else
			{
				++consonantCount;
			}
			
			int pIndex = characters.indexOf(nameChars[i - 1]);
			char next = characters.get(getNextWeightedInt(charOrderWeights[pIndex], charOrderTotalWeights[pIndex]));
			
			if(3 < consonantCount)
			{
				next = characters.get(vowesIndexes[getNextWeightedInt(vowesWeights, vowesTotalWeight)]);
			}
			
			nameChars[i] = next;
			
//			FIXME! Add check (i > 1 || i < size - 2 ??) to avoid this case: "agasenil-"
//			TODO: Add uppercases ("Miti", "Emiro-Rarot", "T'moc")
		}
		
		return new String(nameChars);		
	}
	
	private int getSize()
	{
		int weight = rand.nextInt(sizesTotalWeight);
		int i = 0;
		int size = sizes[0];
		
		while( 0 < (weight -= sizesWeights[i]))
		{
			size = sizes[++i];
		}
		
		return size;
	}
	
	public char getRandomChar()
	{
		int weight = rand.nextInt(charactersTotalWeight);
		int i = 0;
		char character = characters.get(i).charValue();
		
		while(0 < (weight -= charactersWeights[i]))
		{
			character = characters.get(++i).charValue();
		}
		
		return character;
	}
	
	public char getRandomChar(char prev)
	{
		int pIndex = characters.indexOf(prev);
		int[] weights = charOrderWeights[pIndex];
		int weight = rand.nextInt(charOrderTotalWeights[pIndex]);
		
		int i = 0;
		char character = characters.get(i);
		
		while(0 < (weight -= weights[i]))
		{
			character = characters.get(++i);
		}
		
		return character;
	}
	
	private int getNextWeightedInt(int[] weights, int weightTotal)
	{
		int i = 0;
		int weight = rand.nextInt(weightTotal);
		
		while(0 < (weight -= weights[i]))
		{
			++i;
		}
		
		return i;
	}
	
	private boolean isVowel(char c)
	{
		c = Character.toLowerCase(c);
		return 'a' == c || 'e' == c || 'i' == c || 'o' == c || 'u' == c || 'y' == c;
	}
	
	private boolean isVowel(Character c)
	{
		return isVowel(c.charValue());
	}
	
	private class IntPair
	{
		private final int left;
		private final int right;
		
		public IntPair(int left, int right)
		{
			this.left = left;
			this.right = right;
		}

		public int getLeft()
		{
			return left;
		}

		public int getRight()
		{
			return right;
		}
	}
}
