package sfaxx.rndnamegen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Main
{
	private static Character[] letters = new Character[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '-', '\'' };
	private static int[][] charMap = new int[letters.length][letters.length];

	private static int[] charCount = new int[letters.length];
	private static LinkedHashMap<Integer, Integer> lengthes = new LinkedHashMap<>();

	public static void main(String[] args)
	{
//		createCharmap();
		createRandomNames();
	}

	private static void createRandomNames()
	{
		RndNameGen rng = new RndNameGen();
		
		System.out.println(rng.getRandomName());
		
		LinkedList<String> out = new LinkedList<>(); 
		
		for(int i = 0; i < 20; ++i)
		{
			out.add("" + rng.getRandomName());
		}
		
		try
		{
			Files.write(Paths.get("names.rnd"), out, Charset.forName("UTF-8"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private static void createCharmap()
	{
		String line = "";
		InputStream streanCsv = Main.class.getResourceAsStream("/assets/rndnamegen/files/provinces.csv");
		List<Character> listLetters = Arrays.asList(letters);

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(streanCsv)))
		{
			while (null != (line = reader.readLine()))
			{
				if (1 > line.length())
				{
					continue;
				}

				line = line.toLowerCase().substring(0, line.length() - 1);

				char chars[] = line.toCharArray();

				if (!lengthes.containsKey(chars.length))
				{
					lengthes.put(chars.length, 1);
				}
				else
				{
					int v = lengthes.get(chars.length);
					lengthes.put(chars.length, ++v);
				}

				for (int i = 0; i < chars.length - 1; ++i)
				{
					if (' ' == chars[i] || ' ' == chars[i + 1])
					{
						continue;
					}

					if (listLetters.contains(chars[i]))
					{
						int curIndex = listLetters.indexOf(chars[i]);

						if (listLetters.contains(chars[i + 1]))
						{
							int nextIndex = listLetters.indexOf(chars[i + 1]);

							charMap[curIndex][nextIndex]++;
							charCount[curIndex]++;
						}
						else
						{
							System.out.printf("%c in not in charmap!", chars[i + 1]);
						}
					}
					else
					{
						System.out.printf("%c in not in charmap!", chars[i]);
					}
				}

				if (listLetters.contains(chars[chars.length - 1]))
				{
					int i = listLetters.indexOf(chars[chars.length - 1]);
					charCount[i]++;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		lengthes = sortMap(lengthes);

		List<String> output = new ArrayList<>();

		line = letters[0].toString();

		for (int i = 1; i < letters.length; ++i)
		{
			line += "," + letters[i].charValue();
		}

		output.add(line);

		line = "" + charCount[0];

		for (int i = 1; i < letters.length; ++i)
		{
			line += "," + charCount[i];
		}

		output.add(line);

		output.add("<");

		for (Map.Entry<Integer, Integer> entry : lengthes.entrySet())
		{
			output.add(entry.getKey() + "," + entry.getValue());
		}

		output.add(">");

		for (int c = 0; c < charMap.length; ++c)
		{
			line = "" + charMap[c][0];

			for (int n = 1; n < charMap[c].length; ++n)
			{
				line += "," + charMap[c][n];
			}

			output.add(line);
		}

		Path file = Paths.get("charmap.map");

		try
		{
			Files.write(file, output, Charset.forName("UTF-8"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static LinkedHashMap<Integer, Integer> sortMap(Map<Integer, Integer> map)
	{
		List<Map.Entry<Integer, Integer>> list = new LinkedList<Map.Entry<Integer, Integer>>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>()
		{
			public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2)
			{
				return (o1.getKey().intValue() - o2.getKey().intValue());
			}
		});

		LinkedHashMap<Integer, Integer> result = new LinkedHashMap<Integer, Integer>();
		for (Map.Entry<Integer, Integer> entry : list)
		{
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}
}
